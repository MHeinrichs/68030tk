# 68030-TK
 
This is a 68030 CPU turbo-card for the 68000-CPU Slot of an Amiga 500, 1000 and 2000 (It doesn't fit into a CDTV :( ). Its basicly build for an Amiga but could be used elsewhere.

![CPU under heavy debugging](https://gitlab.com/MHeinrichs/68030tk/raw/master/Pics/02-Setup2-K.jpg)

The CPU runs at 25, 33, 40 or 50MHz and must be a PGA type. An optional PLCC FPU 68881/2 can be installed and will be autodetected. A VG96-Connector provides most of the signals to an external expansion card, which could override the 68k-Cycles. 

## Things to improve
The controll logic is done by a lattice i4m-cpld, which I used because it is the last 5V CPLD available. However, this cpld seems to drive the outputs only to 4V and other designs have proven  3.3V/5V mixed systems are stabile, too. Therefore I'll switch to xilinx in a new version of this layout.

The VG96-connector has a bad distribution of VCC/GND signals and the 64MB SD-RAM card suffers from EMV-interfrences. This connector is simply not the best choice for this project :( .


**DISCLAIMER!!!!**  
**THIS IS A HOBBYIST PROJECT! I'M NOT RESPONSIBLE IF IT DOESN'T WORK OR DESTROYS YOUR VALUABLE DATA!**

## License
1. Do whatever you want with this, if you do it for private use and mention this work as origin. 
2. If you want to make money out of this work, ask me first!
3. If you copy parts of this work to your project, mention the source.
4. If you use parts of this work for closed source, burn in hell and live in fear!

## Theory of operation
* This PCB has four layers and consists of the CPU, a Clock source, a Lattice CPLD, some bus bufferes and a multiplexer to translate 16bit DMA cycles to 32 Bit DMA-Cycles. 
* The CPLD does various things:
    * It runs the bus translation betweed the 68000-host and the 68030 CPU
    * It generates the E-Clock and controlls the 6800-preipheral cycles
    * It does the DMA controll
    * It controlls the FPU or generates bus-errors, if no FPU is available
    * It serves 32bit cyclesto the VG96-connector, if an expansion card is connected and accessed. 

## Which expansions are available?
Currently a [4MB SRAM with IDE](https://gitlab.com/MHeinrichs/68030tk-SRAM_IDE-interface) is available. 
An [64MB SD-RAM](https://gitlab.com/MHeinrichs/68030-RAM-IDE) Version is there but still suffers from EMV-issues due to the inapropriate VG96 connector.

## What is in this repository?
This repository consists of several directories:
* Layout and PCB: The board, schematic and bill of material (BOM) are in this directory. 
* Logic: Here the code, project files and pin-assignments for the CPLD is stored
* Pics: Some pictures of this project
* root directory: the readme

## How to open the PCB-files?
You find the eagle board and schematic files (brd/sch) for the pcb. The software can be [downloaded](https://www.autodesk.com/products/eagle/overview) for free after registration. KiCad can import these files too. But importers are not 100% reliable... 

## How to make a PCB?
You can generate Gerber-files from the brd files according to the specs of your PCB-manufactor. However, you can try to use the Gerber-files provided. Some specs: min trace width: 0.15mm/6mil, min trace dist: 0.15mm/6mil, min drill: 0.3mm

**ATTENTION: THE PCB has 4 layers!**

## How to get the parts?
Most parts can be found on digikey or mouser or a parts supplier of your choice. The CPU is the most difficult part to get. Be aware of ebay! A lot of fake CPUs are offered! A list of parts is found in the file [68030-TK-V09f.txt](https://gitlab.com/MHeinrichs/68030tk/raw/master/Layout%20and%20PCB/68030-TK-V09f.txt)

## How to programm the board?
The CPLD must be programmed via Lattice ispVM and an apropriate JTAG-dongle. The JED-File is provided. 

## It doesn't work! How to I get help?
For support visit the [a1k-forum](https://www.a1k.org/forum/showthread.php?t=44245). Yes, you need to register and it's German, but writing in English is accepted. For bug-reports write a ticket here ;). 

